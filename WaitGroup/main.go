package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	const count = 5
	jobsLeft := count
	done := make(chan int)

	rand.Seed(time.Now().UnixNano())

	for i := 0; i < count; i++ {
		go Add(done, i)()
	}

	Wait(done, &jobsLeft)
}

func Add(done chan int, i int) func() {
	return func() {
		time.Sleep(time.Second * time.Duration(rand.Intn(3)))
		done <- i
	}
}

func Wait(done chan int, jobsLeft *int) {

	for j := range done {
		*jobsLeft--

		fmt.Printf("Job ID %d done \n", j)

		if *jobsLeft == 0 {
			fmt.Println("All jobs completed!")
			break
		}
	}
}
