package tree

import (
	"testing"
)

type testPair struct {
	values []int
	isBalanced bool
}

var tests = []testPair{
	{ []int{2, 1, 5, 3, 7}, true },
	{ []int{7, 9, 4, 2, 3, 1}, false },
	{ []int{1, 2, 7, 3, 5}, false },
	{ []int{7, 4, 2}, false },
	{ []int{7}, true },
}

func TestTree_IsBalanced (t *testing.T) {
	for _, pair := range tests {

		tree := Tree{}

		for i := 0; i < len(pair.values); i++ {
			tree.Insert(pair.values[i])
		}

		isBalanced := tree.IsBalanced()

		if isBalanced != pair.isBalanced {
			t.Error(
				"For", pair.values,
				"expected", pair.isBalanced,
				"got", isBalanced,
			)
		}

	}
}

func TestTree_Lock(t *testing.T) {
	values := []int{5, 3, 7}

	tree := Tree{}

	for i := 0; i < len(values); i++ {
		tree.Insert(values[i])
	}

	tree.Lock(7);

	if !tree.Root.Right.IsLocked {
		t.Error("Expected true, got ", tree.Root.Right.IsLocked)
	}
}

//Check that node cannot be locked, if any direct child is locked
func TestTree_Lock2(t *testing.T) {
	values := []int{5, 3, 7}

	tree := Tree{}

	for i := 0; i < len(values); i++ {
		tree.Insert(values[i])
	}

	tree.Lock(7); //child locking
	tree.Lock(5); //root locking

	if tree.Root.IsLocked {
		t.Error("Expected false, got ", tree.Root.IsLocked)
	}
}

//Check that node cannot be locked, if any indirect child is locked
func TestTree_Lock3(t *testing.T) {
	values := []int{5, 3, 7, 2}

	tree := Tree{}

	for i := 0; i < len(values); i++ {
		tree.Insert(values[i])
	}

	tree.Lock(2); // child locking
	tree.Lock(5); // root locking

	if tree.Root.IsLocked {
		t.Error("Expected false, got ", tree.Root.IsLocked)
	}
}

//Check that node cannot be locked, if direct parent is locked
func TestTree_Lock4(t *testing.T) {
	values := []int{5, 3, 7}

	tree := Tree{}

	for i := 0; i < len(values); i++ {
		tree.Insert(values[i])
	}

	tree.Lock(5); // root locking
	tree.Lock(7); // child locking

	if tree.Root.Right.IsLocked {
		t.Error("Expected false, got ", tree.Root.Right.IsLocked)
	}
}

//Check that node cannot be locked, if indirect parent is locked
func TestTree_Lock5(t *testing.T) {
	values := []int{5, 3, 7, 2}

	tree := Tree{}

	for i := 0; i < len(values); i++ {
		tree.Insert(values[i])
	}

	tree.Lock(5); // root locking
	tree.Lock(2); // child locking

	if tree.Root.Left.Left.IsLocked {
		t.Error("Expected false, got ", tree.Root.Left.Left.IsLocked)
	}
}

func TestTree_Unlock(t *testing.T) {
	values := []int{5, 3, 7}

	tree := Tree{}

	for i := 0; i < len(values); i++ {
		tree.Insert(values[i])
	}

	tree.Lock(3);
	tree.Unlock(3);

	if tree.Root.Left.IsLocked {
		t.Error("Expected false, got ", tree.Root.Left.IsLocked)
	}
}

//Check that the function doesn't change node status
func TestTree_Unlock2(t *testing.T) {
	values := []int{5, 3, 7}

	tree := Tree{}

	for i := 0; i < len(values); i++ {
		tree.Insert(values[i])
	}

	tree.Lock(5);
	tree.Unlock(3);

	if tree.Root.Left.IsLocked {
		t.Error("Expected false, got ", tree.Root.Left.IsLocked)
	}
}
