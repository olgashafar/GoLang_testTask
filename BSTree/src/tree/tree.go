package tree

import (
	"fmt"
	"strings"
)

type Node struct {
	Value    int
	IsLocked bool
	Parent   *Node
	Left     *Node
	Right    *Node
}

func (n *Node) Insert(value int) {
	switch {
	case value == n.Value:
		return
	case value < n.Value:
		if n.Left == nil {
			n.Left = &Node{Value: value, Parent: n}
		} else {
			n.Left.Insert(value)
		}

	case value > n.Value:
		if n.Right == nil {
			n.Right = &Node{Value: value, Parent: n}
		} else {
			n.Right.Insert(value)
		}
	}

	return
}

func (n *Node) Dump(i int, lr string) {
	if n == nil {
		return
	}

	indent := ""

	if i > 0 {
		indent = strings.Repeat(" ", (i-1)*5) + "+" + lr + "--"
	}

	isLocked := "unlocked"

	if n.IsLocked {
		isLocked = "locked"
	}

	fmt.Printf("%s%d : %s\n", indent, n.Value, isLocked)
	n.Left.Dump(i+1, "L")
	n.Right.Dump(i+1, "R")

	return
}

func (n *Node) getHeight(h int, min, max *int) {
	if n == nil {
		return
	}

	h++

	if n.Left == nil && n.Right == nil {
		if *min == 0 || *min > h {
			*min = h
		}

		if *max == 0 || *max < h {
			*max = h
		}

		if (*max - *min) > 1 {
			return
		}
	}

	n.Left.getHeight(h, min, max)
	n.Right.getHeight(h, min, max)

	return
}

func (n *Node) find(id int) *Node {
	if n == nil {
		return nil
	}

	switch {
	case id == n.Value:
		return n
	case id < n.Value:
		return n.Left.find(id)
	default:
		return n.Right.find(id)
	}

}

func (n *Node) isParentLocked() bool {
	if n == nil {
		return false
	}

	switch {
	case n.IsLocked:
		return true
	default:
		return n.Parent.isParentLocked()
	}
}

func (n *Node) isChildrenLocked() bool {
	if n == nil {
		return false
	}

	switch {
	case n.IsLocked:
		return true
	case n.Left != nil:
		return n.Left.isChildrenLocked()
	default:
		return n.Right.isChildrenLocked()
	}
}

func (n *Node) changeState(c *Node, state bool) {
	if c == nil {
		return
	}

	prefix := ""
	if !state {
		prefix = "un"
	}

	if c.IsLocked == state {
		fmt.Printf("Node %d is already %slocked.\n", c.Value, prefix)
		return
	}

	if c.Parent.isParentLocked() {
		fmt.Printf("Can't %slock node %d, it's parent is locked.\n", prefix, c.Value)
		return
	}

	if c.Left.isChildrenLocked() || c.Right.isChildrenLocked() {
		fmt.Printf("Can't %slock node %d, it's children is locked.\n", prefix, c.Value)
		return
	}

	c.IsLocked = state
	fmt.Printf("Node %d was %slocked.\n", c.Value, prefix)
}

type Tree struct {
	Root *Node
}

func (t *Tree) Insert(value int) {
	if t.Root == nil {
		t.Root = &Node{Value: value}
	}
	t.Root.Insert(value)
}

func (t *Tree) Dump() {

	fmt.Println("\nTree:")
	t.Root.Dump(0, "")
}

func (t *Tree) getHeight(min, max *int) {

	if t.Root.Left == nil || t.Root.Right == nil {
		*min = 1
	}

	t.Root.getHeight(0, min, max)
}

func (t *Tree) IsBalanced() bool {
	min, max := 0, 0

	t.getHeight(&min, &max)

	if (max - min) > 1 {
		return false
	} else {
		return true
	}
}

func (t *Tree) Lock(id int) {
	if t.Root == nil {
		return
	}

	found := t.Root.find(id)

	if found != nil {
		t.Root.changeState(found, true)
	} else {
		fmt.Printf("Node %d is not found.\n", id)
		return
	}
}

func (t *Tree) Unlock(id int) {
	if t.Root == nil {
		return
	}

	found := t.Root.find(id)

	if found != nil {
		t.Root.changeState(found, false)
	} else {
		fmt.Printf("Node %d is not found.\n", id)
		return
	}

}
