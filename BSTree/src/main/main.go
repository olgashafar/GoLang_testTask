package main

import (
	"tree"
	"fmt"
)

func main() {

	values := []int{2, 1, 5, 3, 7} //balanced tree

	tree := &tree.Tree{}

	for i := 0; i < len(values); i++ {
		tree.Insert(values[i])
	}

	if tree.IsBalanced() {
		fmt.Printf("\nTree is unbalanced")
	} else {
		fmt.Printf("\nTree is balanced")
	}

	tree.Lock(7)
	tree.Unlock(7)

	tree.Dump()
}
